package day2;
import java.util.Scanner;
public class tugas6day2 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int pilihan;
        do {
        System.out.println("MENU");
        System.out.println("1. Volume Balok");
        System.out.println("2. Volume Bola");
        System.out.println("3. Hitung Umur");
        System.out.println("4. EXIT");
        System.out.print("Input nomor = ");
        pilihan = input.nextInt();
        if (pilihan == 4){
            break;
        } else if ((pilihan > 4) || (pilihan <= 0)){
            System.out.println("Pilihan menu tidak ada");
        } else  System.out.println("Menu " + pilihan);

        if (pilihan == 1){
            System.out.print("panjang = ");
            int panjangBalok = input.nextInt();
            System.out.print("lebar = ");
            int lebarBalok = input.nextInt();
            System.out.print("tinggi = ");
            int tinggiBalok = input.nextInt();
            System.out.println("volume balok adalah " + panjangBalok*lebarBalok*tinggiBalok);
        }
        if (pilihan == 2) {
            System.out.print("pi = ");
            float pi = input.nextFloat();
            System.out.println("jari-jari = ");
            int jariJari = input.nextInt();
            float rumusBola = 4*pi*(jariJari*jariJari*jariJari)/3;
            System.out.println("volume bola adalah " + rumusBola);
        }
        if (pilihan == 3) {
            System.out.print("masukan tahun lahir = ");
            pilihan = input.nextInt();
            int usia = 2022 - pilihan;
            System.out.println("usia kamu adalah " + usia);
        }   

    } while (pilihan >= 0);
    input.close();
    }
}
