public class tugas1 {
    public static void main(String[] args) {

        // ini saya mendeklarasikan terlebih dahulu String
        String nama="Peter";
        // ini saya mendeklarasikan terlebih dahulu String
        String kelamin="laki-laki";
        // ini saya mendeklarasikan terlebih dahulu type byte agar kecil
        byte umur=17;
        // ini saya mendeklarasikan terlebih dahulu type float agar kecil
        Float nilai=8.5f;
        
        //bagian ini saya barulah menggabungkan antara keterangan dan deklarasi
        System.out.println("Seorang siswa " + kelamin + "bernama " + nama + ", berumur " + umur + "tahun, memiliki nilai rata-rata " + nilai);
    }
    
}
