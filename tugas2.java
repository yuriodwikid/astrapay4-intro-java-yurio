public class tugas2 {

    public static void main(String[] args) {

        //bagian ini saya mendeklarasikan boolean
        boolean myBoolean = true;
        //bagian ini saya mendeklarasikan byte
        byte myByte = 120;
        //bagian ini saya mendeklarasikan short
        Short myShort = 240;
        //bagian ini saya mendeklarasikan integer
        int myInteger = 2000;
        //bagian ini saya mendeklarasikan long
        long myLong = 10000L;
        //bagian ini saya mendeklarasikan double
        double myDouble = 64.64;
        //bagian ini saya mendeklarasikan float
        float myFloat = 16.16f;
        //bagian ini saya mendeklarasikan char
        char myChar = 'Z';
        //bagian ini saya mendeklarasikan String
        String myString = "G2Academy";

        //disini saya barulah print hasil deklarasi
        //disini saya print keterangan
        System.out.println("Boolean");
        //mengeluarkan hasil deklarasi
        System.out.println(myBoolean);
        //disini saya print keterangan
        System.out.println("Byte");
        //mengeluarkan hasil deklarasi
        System.out.println(myByte);
        //disini saya print keterangan
        System.out.println("Short");
        System.out.println(myShort);
        //disini saya print keterangan
        System.out.println("Integer");
        //mengeluarkan hasil deklarasi
        System.out.println(myInteger);
        //disini saya print keterangan
        System.out.println("Long");
        //mengeluarkan hasil deklarasi
        System.out.println(myLong);
        //disini saya print keterangan
        System.out.println("Double");
        //mengeluarkan hasil deklarasi
        System.out.println(myDouble);
        //disini saya print keterangan
        System.out.println("Float");
        //mengeluarkan hasil deklarasi
        System.out.println(myFloat);
        //disini saya print keterangan
        System.out.println("Char");
        //mengeluarkan hasil deklarasi
        System.out.println(myChar);
        //disini saya print keterangan
        System.out.println("String");
        //mengeluarkan hasil deklarasi
        System.out.println(myString);


    }


    
}
