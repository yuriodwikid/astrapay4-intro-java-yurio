package day2;

import java.util.Scanner;

public class tugas5day2 {

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        String m1 = "Menu 1";
        String m2 = "Menu 2";
        String m3 = "Menu 3";
        String m4 = "Exit";
        int pilihan;

        do {
        System.out.println("MENU");
        System.out.println("1. cetak " + m1);
        System.out.println("2. cetak " + m2);
        System.out.println("3. cetak " + m3);
        System.out.println(m4);
        System.out.print("Input nomor = ");
        pilihan = input.nextInt();
        if (pilihan == 4){
            break;
        } else if (pilihan > 4){
            System.out.println("Pilihan menu tidak ada");
        } else  System.out.println("Menu " + pilihan);
    } while (pilihan >= 0);
    input.close();
    }
    
}
