import java.util.Scanner;

public class tugas4 {
    public static void main(String[] args) {

        //ini saya seperti biasa mendeklarasikan terlebih dahulu
        //input scanner terlebih dahulu
        Scanner input = new Scanner(System.in);
        //ini saya print keterangan
        System.out.print("panjang = ");
        //agar hasil dapat di rumuskan, saya deklarasikan ke number1
        int number1 = input.nextInt();
        //ini saya print keterangan
        System.out.print("lebar = ");
        //agar hasil dapat di rumuskan, saya deklarasikan ke number2
        int number2 = input.nextInt();
        //ini saya print keterangan
        System.out.print("tinggi = ");
        //agar hasil dapat di rumuskan, saya deklarasikan ke number3
        int number3 = input.nextInt();
        //ini saya print keterangan
        System.out.println("volume balok adalah " + number1*number2*number3);
        
        //ini saya print keterangan
        System.out.print("pi = ");
        //agar hasil dapat di rumuskan, saya deklarasikan ke number4
        float number4 = input.nextFloat();
        //ini saya print keterangan
        System.out.println("jari-jari = ");
        //agar hasil dapat di rumuskan, saya deklarasikan ke number4
        int number5 = input.nextInt();
        //agar mempermudah, saya rumuskan terlebih dahulu
        float rumusBola = 4*number4*(number5*number5*number5)/3;
        //ini saya print keterangan dan mengambil rumus
        System.out.println("volume bola adalah " + rumusBola);
    
        //jangan lupa di close agar effisiensi memory
    input.close();
    }
    
}
