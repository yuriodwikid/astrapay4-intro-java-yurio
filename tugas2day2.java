package day2;

import java.util.Scanner;

public class tugas2day2 {
        public static void main(String[] args) {
            
            Scanner input = new Scanner(System.in);

            System.out.println("Choose an operator: +, -, *, or : (ketik salah satu operator)");
            String hitung = input.next();
            System.out.println("Input First Number = " );
            float nilai1 = input.nextFloat();
            System.out.println("Input Second Number = ");
            float nilai2 = input.nextFloat();
    
            float jumlah = nilai1 + nilai2;
            float kurang = nilai1 - nilai2;
            float kali = nilai1 * nilai2;
            float bagi = nilai1 / nilai2;
            switch (hitung){
                case "+" :
                    System.out.println("hasil = " + jumlah);
                    break;
                case "-" :
                    System.out.println("hasil = " + kurang);
                    break;
                case "*" :
                    System.out.println("hasil = " + kali);
                    break;
                case ":" :
                    System.out.println("hasil = " + bagi);
                    break;
                default:
                    System.out.println("tidak di temukan");
            }
            input.close();
        }
    }
