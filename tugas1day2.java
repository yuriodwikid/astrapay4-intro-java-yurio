package day2;

import java.util.Scanner;

public class tugas1day2 {
    public static void main(String[] args) {
        System.out.println("\n\n\n");
        Scanner input = new Scanner(System.in);
        System.out.print("masukan nama = ");
        String nama = input.nextLine();
        System.out.print("masukan tanggal lahir = ");
        int tanggalLahir = input.nextInt();
        System.out.print("masukan tanggal bulan lahir = ");
        int bulanLahir = input.nextInt();
        System.out.print("masukan tanggal tahun lahir = ");
        int tahunLahir = input.nextInt();
        int umur = 2022-tahunLahir;
        
        System.out.println("\n");
        if (bulanLahir == 1) {
            System.out.println("Nama saya " + nama + ", lahir " + tanggalLahir + " Januari " + tahunLahir + " berumur " + umur);
        }
        else if (bulanLahir == 2) {
            System.out.println("Nama saya " + nama + ", lahir " + tanggalLahir + " Februari " + tahunLahir + " berumur " + umur + ".");
        }
        else if (bulanLahir == 3) {
            System.out.println("Nama saya " + nama + ", lahir " + tanggalLahir + " Maret " + tahunLahir + " berumur " + umur + ".");
        }
        else if (bulanLahir == 4) {
            System.out.println("Nama saya " + nama + ", lahir " + tanggalLahir + " April " + tahunLahir + " berumur " + umur + ".");
        }
        else if (bulanLahir == 5) {
            System.out.println("Nama saya " + nama + ", lahir " + tanggalLahir + " Mei " + tahunLahir + " berumur " + umur + ".");
        }
        else if (bulanLahir == 6) {
            System.out.println("Nama saya " + nama + ", lahir " + tanggalLahir + " Juni " + tahunLahir + " berumur " + umur + ".");
        }
        else if (bulanLahir == 7) {
            System.out.println("Nama saya " + nama + ", lahir " + tanggalLahir + " Juli " + tahunLahir + " berumur " + umur + ".");
        }
        else if (bulanLahir == 8) {
            System.out.println("Nama saya " + nama + ", lahir " + tanggalLahir + " Agustus " + tahunLahir + " berumur " + umur + ".");
        }
        else if (bulanLahir == 9) {
            System.out.println("Nama saya " + nama + ", lahir " + tanggalLahir + " September " + tahunLahir + " berumur " + umur + ".");
        }
        else  if (bulanLahir == 10) {
            System.out.println("Nama saya " + nama + ", lahir " + tanggalLahir + " Oktober " + tahunLahir + " berumur " + umur + ".");
        }
        else if (bulanLahir == 11) {
            System.out.println("Nama saya " + nama + ", lahir " + tanggalLahir + " November " + tahunLahir + " berumur " + umur + ".");
        } else{
            System.out.println("Nama saya " + nama + ", lahir " + tanggalLahir + " Desember " + tahunLahir + " berumur " + umur + ".");
        }
        
        System.out.println("\n\n\n");
        input.close();
    }
}
