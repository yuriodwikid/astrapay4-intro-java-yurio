public class tugas3 {

    public static void main(String[] args) {

    System.out.println("//////////////////////////////////");

    // deklarasi integer panjang
    int panjang = 12;
    //deklarasi integer lebar
    int lebar = 8;
    //deklarasi integer rumus
    int luasPersegiPanjang = panjang * lebar;
    
    //deklarasi float alas
    float alas = 7.0f;
    //deklarasi float tinggi
    float tinggi = 3.5f;
    //deklarasi float rumus
    float luasSegitiga = alas * tinggi;
    //deklarasi float rumus dan hasil akhir
    float luasSegitigaFix = luasSegitiga/2;
    
    //setelah deklarasi, baru lah print hasil deklarasi luas persegi panjang
    System.out.println("luas persegi panjang adalah " + luasPersegiPanjang);
    //setelah deklarasi, baru lah print hasil deklarasi luas segitiga
    System.out.println("luas segitiga adalah " + luasSegitigaFix);
    //setelah deklarasi, baru lah print hasil boolean antara hasil luas persegi panjang dan segitiga
    System.out.println(luasPersegiPanjang > luasSegitigaFix);

    //disini saya tabrak semua deh kak,hehehe 3x decrement trus langsung 6x decrement
    --luasPersegiPanjang;
    --luasPersegiPanjang;
    --luasPersegiPanjang;
    //menjelaskan hasil decrement
    System.out.println(luasPersegiPanjang);
    ++luasPersegiPanjang;
    ++luasPersegiPanjang;
    ++luasPersegiPanjang;
    ++luasPersegiPanjang;
    ++luasPersegiPanjang;
    ++luasPersegiPanjang;
    //menjelaskan hasil increment
    System.out.println(luasPersegiPanjang);
    
    
    System.out.println("//////////////////////////////////");
    }
}
